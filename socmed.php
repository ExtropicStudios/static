<?php
$socmeds = [
	'images/deviantart.jpg' => 'https://extropic-engine.deviantart.com/gallery',
	'images/diaspora.jpg' => 'https://poddery.com/people/e2c6cbd2516f213b',
	'images/ello.jpg' => 'https://ello.co/extropic-engine',
	'images/instagram.jpg' => 'https://www.instagram.com/extropic_engine/',
	'images/mastodon.jpg' => 'https://toot.cafe/web/accounts/8156',
	'images/tumblr.jpg' => 'https://hematite-drop.tumblr.com',
	'images/youtube.jpg' => 'https://www.youtube.com/user/ExtropicEngine',
];

    function shuffle_assoc(&$array) {
			        $keys = array_keys($array);

							        shuffle($keys);

							        foreach($keys as $key) {
												            $new[$key] = $array[$key];
																		        }

											        $array = $new;

											        return true;
															    }

shuffle_assoc($socmeds);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width initial-scale=1'>
    <title>extropic studios</title>
		<link rel="icon" type="image/png" href="/favicons/main.png">
		<style>
			img {
				max-height: 100px;
			}
			.linkbox {
				border: 3px double gray;
				margin: 0.5rem;
				display: inline-block;
			}
			.linkbox:hover {
				background: rgba(0, 0, 255, 0.5);
			}
			img:hover {
				opacity: 0.8;
			}
		</style>
  </head>
<body>
<?php
	foreach ($socmeds as $img => $link) {
		echo "<span class='linkbox'><a href='{$link}'><img src='{$img}' /></a></span>\n";
	}
	?>
</body>
