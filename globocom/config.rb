PROJECT_NAME = 'GloboCom'
BASEDIR = File.expand_path(File.dirname(__FILE__))
DBPATH = "#{BASEDIR}/db/#{PROJECT_NAME}.db"
VERSION = '1'

if ENV['NFSN_SITE_NAME'] == 'extropicstudios' then
  # hardcoded for now
  SITE_URL = 'https://www.extropicstudios.com'
  DB_USER = 'globocom'
  DB_PASS = '{secret}'
  DB_HOST = 'extropicstudios.db'
  DB_NAME = 'globocom'
else
  SITE_URL = 'http://localhost'
  DB_USER = 'globocom'
  DB_PASS = 'password'
  DB_HOST = 'localhost'
  DB_NAME = 'globocom'
end

BASE_URL = "#{SITE_URL}/#{PROJECT_NAME.downcase}"
