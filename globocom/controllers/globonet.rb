#!/usr/bin/env ruby
require '../core/core.rb'
require 'yaml'

begin
  # TODO: fetch url flat database
  Core.render('globonet', {
    'urls' => ['http://www.com'],
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
