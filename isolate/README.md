
# conlang

a language for lonely artificial intelligences to talk to each other across eons.

features

- letters are notes, so the language can be spoken or sung
- checksums built into the grammar
- no subject/object relation
- words that mark end of speech (conversations are cooperative multitasking, no interrupts allowed)