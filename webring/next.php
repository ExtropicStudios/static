<?php
require_once ('sites.php');

if (!isset($index)) {
  include('random.php');
} else {
  $index++;
  if ($index >= count($sites)) {
    $index = 0;
  }

  $url = $sites[$index];

  Header( "HTTP/1.1 302 Moved Temporarily" );
  Header( "Location: $url" );
}