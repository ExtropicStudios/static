<?php
require_once ('sites.php');

$rand = rand(0, count($sites) - 1);

while (!empty($index) && $rand == $index) {
  $rand = rand(0, count($sites) - 1);
}

$url = $sites[$rand];

Header( "HTTP/1.1 302 Moved Temporarily" );
Header( "Location: $url" );
