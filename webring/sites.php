<?php
$lookup = array(
  'extropicstudios' => 0,
  'tangentbot' => 1,
  'modestcomics' => 2,
  'raveradbury' => 3,
);
$index = $lookup[$_GET['from']];
$sites = array(
  0 => 'https://www.extropicstudios.com/nothings.html',
  1 => 'http://www.tangentbot.com',
  2 => 'http://www.modestcomics.com',
  3 => 'http://www.raveradbury.com',
);
?>
