<?php
require_once ('sites.php');

if (!isset($index)) {
  include('random.php');
} else {
  $index--;
  if ($index < 0) {
    $index = count($sites) - 1;
  }

  $url = $sites[$index];

  Header( "HTTP/1.1 302 Moved Temporarily" );
  Header( "Location: $url" );
}
