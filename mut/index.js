window.onload = function () {
    var container = document.getElementById("container");

    if (localStorage.api_token === undefined || localStorage.instance === undefined) {
        container.innerHTML = "<input id=\"instance\" type=\"text\" placeholder=\"Instance Name\"/><br/>" +
        "<input id=\"api_token\" type=\"text\" placeholder=\"Paste your API token here!\"/><br/>" +
        "<button onclick=saveApiToken()>Go!</button><br/>" +
        "(To get an API token, go to <your instance>/settings/application/new, enter any name and hit submit. " +
        "Then click on the application name and copy/paste the thing that says 'Your access token'.)";
    } else {
        runApp(localStorage.instance, localStorage.api_token);
    }
};

function saveApiToken() {
    var newToken = document.getElementById("api_token").value;
    localStorage.api_token = newToken;

    var newInstance = document.getElementById("instance").value;
    localStorage.instance = newInstance;
    runApp(newInstance, newToken);
}

function runApp(my_instance, api_token) {
    var api = new MastodonAPI({
        instance: "https://" + my_instance,
        api_user_token: api_token,
    });

    var container = document.getElementById("container");

    if (localStorage.followers === "" || localStorage.followers === undefined) { localStorage.followers = JSON.stringify([]); }
    if (localStorage.following === "" || localStorage.following === undefined) { localStorage.following = JSON.stringify([]); }
    if (localStorage.mutes === "" || localStorage.mutes === undefined) { localStorage.mutes = JSON.stringify([]); }

    var mutuals = [];
    var fans = [];
    var idols = [];
    var muted_mutuals = [];
    var muted_idols = [];

    update();
    render();
    api.get("accounts/verify_credentials", {}, (account) => {
        getAll("accounts/" + account.id + "/followers", {}, (followers) => {
            localStorage.followers = JSON.stringify(followers);
            update();
            render();
            getAll("accounts/" + account.id + "/following", {}, (following) => {
                localStorage.following = JSON.stringify(following);
                update();
                render();
                getAll("mutes", {}, (mutes) => {
                    localStorage.mutes = JSON.stringify(mutes);
                    update();
                    render();
                });
            });
        });
    });

    function update() {
        followers = JSON.parse(localStorage.followers);
        following = JSON.parse(localStorage.following);
        mutes = JSON.parse(localStorage.mutes);
        mutuals = [];
        fans = [];
        idols = [];
        muted_mutuals = [];
        muted_idols = [];

        followers.forEach((fer) => {
            if (following.find(fing => fer.id === fing.id)) {
                if (mutes.find(m => m.id === fer.id)) {
                    muted_mutuals.push(fer);
                } else {
                    mutuals.push(fer);
                }
            } else {
                fans.push(fer);
            }
        });

        following.forEach((fing) => {
            if (!followers.find(fer => fer.id === fing.id)) {
                if (mutes.find(m => m.id === fing.id)) {
                    muted_idols.push(fing);
                } else {
                    idols.push(fing);
                }
            }
        });
    }

    function render() {
        container.innerHTML = "";
        writeAccounts("Fans", fans);
        writeAccounts("Idols", idols);
        writeAccounts("Mutuals", mutuals);
        writeAccounts("Muted Mutuals", muted_mutuals);
        writeAccounts("Muted Idols", muted_idols);
    }

    function getAll(uri, params, callback) {
        return getAllRecursiveHelper([], uri, params, callback);
    }

    function getAllRecursiveHelper(dataSoFar, uri, params, callback) {
        params.limit = 80;
        api.get(uri, params, (results, textStatus, request) => {
            links = parse_link_header(request.getResponseHeader("Link"));

            if (links.next) {
                var next = links.next.replace("https://" + my_instance + "/api/v1/", "");
                getAllRecursiveHelper(dataSoFar.concat(results), next, params, callback);
            } else {
                callback(dataSoFar.concat(results));
            }
        });
    }

    function writeAccounts(title, accounts) {
        console.log(accounts);
        container.innerHTML += "<hr><h1>" + title + " (" + accounts.length + ")</h1><br>";
        accounts.forEach((s) => {
            if (s.acct.indexOf("@") === -1) {
                s.acct += "@" + my_instance;
            }
            container.innerHTML += "<div class=\"account\">"
              + "<img class=\"avatar\" src=\"" + s.avatar_static + "\">"
              + "<div class=\"account_name\"><a href=\"https://" + my_instance + "/web/accounts/" + s.id + "\">" + s.acct + "</a></div>"
              + "<div class=\"display_name\"><a href=\"https://" + my_instance + "/web/accounts/" + s.id + "\">" + s.display_name + "</a></div>"
              + "</div>";
        });
    }

    function parse_link_header(header) {
        if (header.length == 0) {
            throw new Error("input must not be of zero length");
        }

        // Split parts by comma
        var parts = header.split(",");
        var links = {};
        // Parse each part into a named link
        parts.forEach((p) => {
            var section = p.split(";");
            if (section.length !== 2) {
                throw new Error("section could not be split on ';'");
            }
            var url = section[0].replace(/<(.*)>/, "$1").trim();
            var name = section[1].replace(/rel="(.*)"/, "$1").trim();
            links[name] = url;
        });

        return links;
    }
}