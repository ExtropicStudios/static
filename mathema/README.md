# TODO List

- [ ] Try converting JSON files for [Amazon Ion](https://amznlabs.github.io/ion-docs/index.html)

## books

- [ ] Figure out solution for short stories/comic book issues
- [ ] Generate subpages for authors and series
- [ ] Add short stories from Hainish Cycle

## games

- [ ] Restructure game userpages to be more sane and scalable

## movies

- [X] Add watchlist for movies
- [ ] Rename "video", include TV shows / talks

## podcasts

- [ ] Convert podcasts to use .hbs
